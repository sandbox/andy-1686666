<?php
/**
 * @file
 * commerce_calcprice.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_calcprice_default_rules_configuration() {
  $items = array();
  $items['rules_commerce_calcprice_update_on_save'] = entity_import('rules_config', '{ "rules_commerce_calcprice_update_on_save" : {
      "LABEL" : "Update precalculated price",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "REQUIRES" : [ "commerce_calcprice", "entity" ],
      "ON" : [ "commerce_product_insert", "commerce_product_update" ],
      "DO" : [
        { "commerce_calcprice_recalculate_price_action" : { "commerce_product" : [ "commerce_product" ] } }
      ]
    }
  }');
  return $items;
}
