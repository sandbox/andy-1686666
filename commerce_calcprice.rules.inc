<?php
/**
 * Rules hooks, callbacks and helper functions.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_calcprice_rules_action_info() {
  return array(
    'commerce_calcprice_recalculate_price_action' => array(
      'label' => t("Update precalculated price"),
      'group' => t("Commerce Product"),
      'parameter' => array(
        'commerce_product' => array(
          'type' => 'commerce_product',
          'label' => t("Product"),
        ),
      ),
    ),
  );
}

/**
 * Pre-calculates sell prices for the specified product based on valid rule
 * configurations on the "Calculating product sell price" event.
 *
 * @param $product
 *   The product whose prices should be precalculated.
 *
 * @see commerce_product_pre_calculate_sell_prices()
 */
function commerce_calcprice_recalculate_price_action($product) {

  // Load the sell price calculation event.
  $event = rules_get_cache('event_commerce_product_calculate_sell_price');

  // If there are no rule configurations, leave without further processing.
  if (empty($event)) {
    return array();
  }

  // If the product is valid for pre-calculation...
  if (commerce_product_valid_pre_calculation_product($product)) {
  
    // The price gets clobbered when calculating the line item's sale price, so
    // the original gets backed up here and restored at the end.
    $price = $product->commerce_price;
    
    // Find out what records already exist.
    $language = empty($product->language) ? LANGUAGE_NONE : $product->language;
    $sql = <<<SQL
  SELECT id, module_key
  FROM {commerce_calculated_price}
  WHERE
    entity_id = :product_id AND
    language = :language AND
    entity_type = 'commerce_product' AND
    field_name = 'commerce_price' AND
    delta = 0 AND
    module = 'commerce_product_pricing'
SQL;
    $args = array(
      ':product_id' => $product->product_id,
      ':language' => $language,
    );
    $result = db_query($sql, $args);
    $existing_keys = $result->fetchAllAssoc('module_key');
    
    // Set up any other variables.
    $time = time();
    
    // For each rule key (i.e. set of applicable rule configurations)...
    foreach (commerce_product_pre_calculation_rule_keys() as $key) {
      // Build a product line item and Rules state object.
      $line_item = commerce_product_line_item_new($product);

      $state = new RulesState();
      $vars = $event->parameterInfo(TRUE);
      $state->addVariable('commerce_line_item', $line_item, $vars['commerce_line_item']);

      // For each Rule signified by the current key...
      foreach (explode('|', $key) as $name) {
        // Load the Rule and "fire" it, evaluating its actions without doing
        // any condition evaluation.
        if ($rule = rules_config_load($name)) {
          $rule->fire($state);
        }
      }

      // Also fire any Rules that weren't included in the key because they
      // don't have any conditions.
      foreach ($event as $rule) {
       if (count($rule->conditions()) == 0) {
          $rule->fire($state);
        }
      }

      // Build the record of the pre-calculated price and write it.
      $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

      if (!is_null($wrapper->commerce_unit_price->value())) {
      
        $record = array(
          'module' => 'commerce_product_pricing',
          'module_key' => $key,
          'entity_type' => 'commerce_product',
          'entity_id' => $product->product_id,
          'field_name' => 'commerce_price',
          'language' => $language,
          'delta' => 0,
          'amount' => round($wrapper->commerce_unit_price->amount->value()),
          'currency_code' => $wrapper->commerce_unit_price->currency_code->value(),
          'data' => $wrapper->commerce_unit_price->data->value(),
          'created' => $time,
        );

        // Save the price.
        $primary_keys = array();
        if (isset($existing_keys[$key])) {
          $primary_keys = 'id';
          $record['id'] = $existing_keys[$key]->id;

          // Remove keys after they've been processed so that finally we have a
          // list of records to delete in $existing_keys.
          unset($existing_keys[$key]);
        }
        drupal_write_record('commerce_calculated_price', $record, $primary_keys);
      }
    }
    
    // Remove any stale records.
    $delete_ids = array();
    foreach ($existing_keys as $record) {
      $delete_ids[] = $record->id;
    }
    
    if ($delete_ids) {
      $del_sql = <<<SQL
  DELETE FROM {commerce_calculated_price}
  WHERE id IN (:delete_ids)
SQL;
      db_query($del_sql, array(':delete_ids' => $delete_ids));
    }
    
    // Reset the product's price to what it was originally
    $product->commerce_price = $price;
  } 
}
